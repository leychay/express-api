//
// Router Index
//
const express = require('express')
const routes = express.Router()

// Load 'em up
const User = require("./user")
const Account = require("./account")

// Use 'em up
routes.use('/user', User)
routes.use('/account', Account)

// Export 'em up
module.exports = routes
