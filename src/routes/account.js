//
// Account Router
//
const express = require('express')
// const bodyParser = require('body-parser')
const router = express.Router()

// Log incoming request in Account Router
router.use((req, res, next) => {
  console.log("Account Router is called")
  next()
})
router.get('/me', (req, res) => {
  console.log("My Request:", req.body)
  res.json("I'm in Account Router")
})

module.exports = router
