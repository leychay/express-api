//
// User Router
//
const express = require('express')
// const bodyParser = require('body-parser')
const router = express.Router()

// Log incoming request in User Router
router.use((req, res, next) => {
  console.log("User Router is called")
  next()
})
router.get('/me', (req, res) => {
  console.log("My Request:", req.body)
  res.json("I'm in User Router")
})

router.get('/new-field', (req, res) => {
  console.log("My Request:", req.body)
  res.json("I'm in User - New field")
})

module.exports = router
