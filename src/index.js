// Load Config
require('dotenv').config()

// Init express
const express = require('express')
const app = express()

// Import Routes
const Routes = require("./routes")

// Common constants
const port = process.env.PORT

app.get('/', (req, res) => {
  let version = process.env.APP_VERSION
  console.log(`EXPRESS API VERSION ${version}`)
  res.json(`EXPRESS API VERSION ${version}`)
})

// Load RouteGroup
app.use('', Routes)

//Lezgo
app.listen(port, () => {
  console.log(`Server started on port %s`, port)
})
