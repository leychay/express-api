# Express App in AWS
Bootstrapped Express JS for AWS setup

## Requirements
* Node ^v15.14.0
* NPM ^ v7.7.6 OR Yarn ^v1.22.10

## Installation
```
$ yarn install
## OR
$ npm install
```
## Run 'em
1. Copy `.env.example` to `.env`. `$ cp .env.example .env`
2. Run `node src/index.js`
3. Access via browser at [http://localhost:3000](http://localhost:3000)

## TODO
- [x] Use dotenv
- [x] Sample Routes
- [ ] Controllers and Services
- [ ] JWT Middlewares
- [ ] Gateways for 3rd-party API Integration
- [ ] Dockerize app
- [ ] Gitlab CI
- [ ] AWS ECS setup
